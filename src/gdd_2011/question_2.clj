(ns gdd-2011.question-2
  (:use [gdd-2011.util]))

;; Problem-specific config:
(def *verb-min-length* 7)
(def *verb-fn* bar-char?)

(defn verb? [word]
  (and (>= (count word) *verb-min-length*)
       (*verb-fn* (last word))))

(defn subjunctive-verb? [word]
  (and (verb? word)
       (*verb-fn* (first word))))

(defn verbs [words]
  (filter verb? words))

(defn subjunctive-verbs [words]
  (filter subjunctive-verb? words))

;; Answers:
;; (count (verbs (from "input/text-a.txt")))
;; (count (subjunctive-verbs (from "input/text-a.txt")))
