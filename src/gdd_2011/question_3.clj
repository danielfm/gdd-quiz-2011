(ns gdd-2011.question-3
  (:use [gdd-2011.util]))

(defn words-diff [word-1 word-2]
  (map - (word-weight word-1) (word-weight word-2)))

(defn first-diff [word-1 word-2]
  (some #(when-not (zero? %) %) (words-diff word-1 word-2)))

(defn compare-words [word-1 word-2]
  (if (= word-1 word-2)
    0
    (if-let [d (first-diff word-1 word-2)]
      d
      (- (count word-1) (count word-2)))))

(defn sort-words [words]
  (sort compare-words (distinct words)))

;; Answer:
;; (apply print-str (sort-words (from "input/text-a.txt")))
