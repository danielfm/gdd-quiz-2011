(ns gdd-2011.question-1
  (:use [gdd-2011.util]))

;; Problem-specific config:
(def *prep-length* 3)
(def *prep-fn* bar-char?)          ;; or foo-char?
(def *prep-not-contains-char* \l)

(defn preposition? [word]
  (and (= *prep-length* (count word))
       (*prep-fn* (last word))
       (not (has-char? word *prep-not-contains-char*))))

(defn prepositions [words]
  (filter preposition? words))

;; Answer:
;; (count (prepositions (from "input/text-a.txt")))
