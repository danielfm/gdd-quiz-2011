(ns gdd-2011.question-4
  (:use [gdd-2011.util]))

;; Problem-specific config:
(def *pretty-min* 404169)
(def *pretty-divisible-by* 5)

(defn number [word]
  (reduce + (map #(bigint (* %1 (Math/pow 20 %2)))
                 (word-weight word)
                 (iterate inc 0))))

(defn pretty-number? [word]
  (let [n (number word)]
    (and (> n *pretty-min*) (zero? (mod n *pretty-divisible-by*)))))

(defn pretty-numbers [words]
  (filter pretty-number? words))

;; Answer:
;; (count (pretty-numbers (from "input/text-a.txt")))
