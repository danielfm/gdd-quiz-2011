(ns gdd-2011.util)

;; General config:
(def *chars* "wnzpkgrdqbfjlxvhcsmt")
(def *foo-chars* "vkcjq")

(def *indexed-chars* (zipmap *chars* (iterate inc 0)))

(defn has-char? [word ch]
  ((set word) ch))

(defn foo-char? [ch]
  (has-char? *foo-chars* ch))

(def bar-char? (complement foo-char?))

(defn word-weight [word]
  (map *indexed-chars* word))

(defn from [input-file]
  (re-seq #"\w+" (slurp input-file)))
