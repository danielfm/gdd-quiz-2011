# Clojure - Google Developer Day 2011 Solutions #

The Google Developer Day 2011 quiz is part of the registration process and will
be used as a selection criterion if the number of registrations to the event
exceeds seating capacity.


## Each Quiz Is Unique ##

If you just use my answers in your quiz, you'll probably miss all questions
since each test has its own set of parameters.

See below what changes you need to do to get the correct answers.

### Scroll Texts ###

1. Text A: `input/text-a.txt`
2. Text B: `input/text-b.txt`

### Foo/Bar Chars ###

In `src/gdd_2011/util.clj`:

    (def *foo-chars* "your set of foo letters")

### Prepositions ###

In `src/gdd_2011/question_1.clj`:

    (def *prep-length* x)             ;; Word length must be x
    (def *prep-fn* xxx-char?)         ;; Last char of word must be (xxx-char? char)
    (def *prep-not-contains-char* ch) ;; Word must not contain ch

### Verbs ###

In `src/gdd_2011/question_2.clj`:

    (def *verb-min-length* x) ;; Word length must be at least x
    (def *verb-fn* xxx-char?) ;; Last char of word must be xxx-char?

### Subjunctive Verbs ###

In `src/gdd_2011/question_2.clj`:

    (def *verb-fn* xxx-char?) ;; First char of word must be xxx-char?

### Lexicographical Order ###

In `src/gdd_2011/util.clj`:

    (def *chars* "your set of letters in order")

### Pretty Numbers ###

In `src/gdd_2011/question_4.clj`:

    (def *pretty-min* x)          ;; Number must be greater than x
    (def *pretty-divisible-by* y) ;; Number must be divisible by y


## Example: Answering The First Question ##

Run `lein repl` and type the following commands to see the answer to the first
question:

    (ns lazy-being
      (:use [gdd-2011.util])
      (:use [gdd-2011.question-1]))

    (count (prepositions (from "input/text-b.txt")))
    => w00t


## Prerequisites ##

* [Clojure][clj] 1.2.1;
* [Leiningen][lein] 1.6.1.

[clj]: http://clojure.org/
[lein]: http://github.com/technomancy/leiningen
