(defproject gdd-quiz-2011 "1.0.0"
  :url "http://github.com/danielfm/gdd-quiz-2011"
  :description "Clojure solution for the Google Development Day 2011 quiz."
  :dependencies [ [org.clojure/clojure "1.2.1"] ])
